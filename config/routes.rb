Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'visitors#index'

  match '/about', to: 'pages#about', via: 'get'

  devise_for :users
  resources :users

  get '/controls', to: 'robot#index', as: 'controls'
  get '/set_command', to: 'robot#command', as: 'set_command'
  get '/delete_history', to: 'robot#destroy', as: 'delete_history'

  namespace :api do
    get '/command', to: 'robot#index', as: 'command'
    get '/executed', to: 'robot#update', as: 'executed'
  end


end
