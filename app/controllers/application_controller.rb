class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action :clean_command_history
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  def clean_command_history
    @history = Robot.where.not(id: Robot.all.last)
    if @history.count >= 9000
      @history.delete_all
    end
  end
end
