class Api::RobotController < ApiController

  # GET /api/command
  def index
    @robot = Robot.last
    render json: @robot
  end

  # GET /api/executed
  def update
    @robot = Robot.last
    if @robot.update(:executed => 'true')
      render json: @robot
    else
      render json: @robot.errors, status: :unprocessable_entity
    end
  end

end
