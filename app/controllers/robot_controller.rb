class RobotController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def command
    @sent_command = params[:command]
    @new_command = Robot.new(:command => @sent_command, :executed => 'false')
    @new_command.save
  end

  def destroy
    @message = Robot.clean_history
  end

end
