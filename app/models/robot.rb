class Robot < ApplicationRecord
  self.table_name = "robot"

  def self.clean_history
    @history = self.where.not(id: self.all.last)
    @count = @history.count
    @history.delete_all

    return "#{@count} commands cleaned from history!"
  end
end
