class CreateAdminService
  def call
    user = User.find_or_create_by!(email: ENV.fetch("ADMIN_EMAIL")) do |user|
        user.password = ENV.fetch("ADMIN_PASSWORD")
        user.password_confirmation = ENV.fetch("ADMIN_PASSWORD")
      end
  end
end
