class CreateRobot < ActiveRecord::Migration[5.2]
  def change
    create_table :robot do |t|
      t.string :command
      t.string :executed
    end
  end
end
